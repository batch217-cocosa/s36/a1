// It contains all endpoints for our application

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");


// Routes
// It is responsible for defining or creating endpoints
// All the business logic is done in the controller

router.get("/viewTasks", (req, res) => {
	// Invokes the getAllTasks" function from the "taskController.js" file and sends the result back to the client/postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

});

router.post('/addNewTasks',(req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));

})

router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	})
})

router.put("/updateTask/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


//////////////////////////////////////////////////////////////
////////////////// Activity Portion///////////////////////////
//////////////////////////////////////////////////////////////

router.get("/specificTask/:id", (req, res) => {
	taskController.specificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {
	taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
})




module.exports = router;